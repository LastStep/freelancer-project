import requests
from bs4 import BeautifulSoup

def get_title(url):
  soup = BeautifulSoup(req.get(url).content, 'lxml')
  title = soup.title.get_text()

  with open('out.txt', '+w') as f:
    f.write(title)

  num_spaces, num_upper, num_lower = 0, 0, 0
  for i in title:
    if i == ' ':
      num_spaces += 1
    elif i.isupper():
      num_upper += 1
    elif i.islower():
      num_lower += 1
  return num_spaces, num_upper, num_lower

if __name__ == '__main__':
  url = 'https://europa.eu/european-union/about-eu/countries_en'

  with requests.Session() as req:
    num_spaces, num_upper, num_lower = get_title(url)
  print(num_spaces, num_upper, num_lower)